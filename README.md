## Slides are based on Reveal.js
See https://github.com/hakimel/reveal.js for full documentation.


### Start presentation locally

1. Install [Node.js](http://nodejs.org/)

2. Install [Grunt](http://gruntjs.com/getting-started#installing-the-cli)

4. Clone the reveal.js repository
   ```sh
   $ git clone https://bitbucket.org/josso/react-flux-webpack-slides.git
   ```

5. Navigate to folder
   ```sh
   $ cd react-flux-webpack-slides
   ```

6. Install dependencies
   ```sh
   $ npm install
   ```

7. Serve the presentation and monitor source files for changes
   ```sh
   $ grunt serve
   ```

8. Open <http://localhost:8000> to view your presentation

   You can change the port by using `grunt serve --port 8001`.
